FROM arm32v7/openjdk:8

WORKDIR /minecraft
COPY . .

VOLUME /minecraft
EXPOSE 25565

CMD [ "java", "-jar", "./spigot.jar" ]
